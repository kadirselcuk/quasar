/*
 * Forked from vue-bundle-runner v0.0.3 NPM package
 */

const { extname } = require('path')
const createBundle = require('./lib/create-bundle')

const jsRE = /\.js(\?[^.]+)?$/
const cssRE = /\.css(\?[^.]+)?$/
const queryRE = /\?.*/

/**
 * Creates a mapper that maps components used during a server-side render
 * to async chunk files in the client-side build, so that we can inline them
 * directly in the rendered HTML to avoid waterfall requests.
*/
function createMapper (clientManifest) {
  const map = new Map()

  Object.keys(clientManifest.modules).forEach(id => {
    map.set(id, mapIdToFile(id, clientManifest))
  })

  // map server-side moduleIds to client-side files
  return function mapper (moduleIds) {
    const res = new Set()
    for (let i = 0; i < moduleIds.length; i++) {
      const mapped = map.get(moduleIds[i])
      if (mapped) {
        for (let j = 0; j < mapped.length; j++) {
          const entry = mapped[j]
          if (entry !== void 0) {
            res.add(mapped[j])
          }
        }
      }
    }
    return Array.from(res)
  }
}

function mapIdToFile (id, clientManifest) {
  const files = []
  const fileIndices = clientManifest.modules[id]

  if (fileIndices !== void 0) {
    fileIndices.forEach(index => {
      const file = clientManifest.all[index]

      // only include async files or non-js, non-css assets
      if (
        clientManifest.async.includes(file) ||
        (/\.(js|css)($|\?)/.test(file) === false)
      ) {
        files.push(file)
      }
    })
  }

  return files
}

function normalizeFile (file) {
  const fileWithoutQuery = file.replace(queryRE, '')
  const extension = extname(fileWithoutQuery).slice(1)

  return {
    file,
    extension,
    fileWithoutQuery
  }
}

function ensureTrailingSlash (path) {
  return path === ''
    ? path
    : path.replace(/([^/])$/, '$1/')
}

function createRenderContext ({ clientManifest, publicPath }) {
  return {
    clientManifest,
    publicPath: ensureTrailingSlash(publicPath || clientManifest.publicPath || '/'),
    preloadFiles: (clientManifest.initial || []).map(normalizeFile),
    mapFiles: createMapper(clientManifest)
  }
}

function renderStyles (renderContext, usedAsyncFiles, ssrContext) {
  const initial = renderContext.preloadFiles
  const cssFiles = initial.concat(usedAsyncFiles).filter(({ file }) => cssRE.test(file))

  return (
    // render links for css files
    (
      cssFiles.length
        ? cssFiles.map(({ file }) => `<link rel="stylesheet" href="${renderContext.publicPath}${file}">`).join('')
        : ''
    ) +
    // ssrContext.styles is a getter exposed by vue-style-loader which contains
    // the inline component styles collected during SSR
    (ssrContext.styles || '')
  )
}

function renderScripts(renderContext, usedAsyncFiles) {
  if (renderContext.preloadFiles.length > 0) {
    const initial = renderContext.preloadFiles.filter(({ file }) => jsRE.test(file))
    const async = usedAsyncFiles.filter(({ file }) => jsRE.test(file))

    return [ initial[0] ].concat(async, initial.slice(1))
      .map(({ file }) => `<script src="${renderContext.publicPath}${file}" defer></script>`)
      .join('')
  }

  return ''
}

module.exports = function createRenderer (opts) {
  const renderContext = createRenderContext(opts)
  const { evaluateEntry, rewriteErrorTrace } = createBundle(opts)

  async function runApp(ssrContext) {
    try {
      const entry = await evaluateEntry()
      const app = await entry(ssrContext)
      return app
    }
    catch (err) {
      rewriteErrorTrace(err)
      throw err
    }
  }

  return async function renderToString (ssrContext, renderTemplate) {
    try {
      Object.assign(ssrContext, {
        _modules: new Set(),
        _meta: {},
        _onRenderedList: []
      })

      const app = await runApp(ssrContext)
      const resourceApp = await opts.vueRenderToString(app, ssrContext)
      const usedAsyncFiles = renderContext
        .mapFiles(Array.from(ssrContext._modules))
        .map(normalizeFile)

      ssrContext._onRenderedList.forEach(fn => { fn() })

      Object.assign(ssrContext._meta, {
        resourceApp,
        resourceStyles: renderStyles(renderContext, usedAsyncFiles, ssrContext),
        resourceScripts: renderScripts(renderContext, usedAsyncFiles)
      })

      return renderTemplate(ssrContext)
    }
    catch (err) {
      await rewriteErrorTrace(err)
      throw err
    }
  }
}
